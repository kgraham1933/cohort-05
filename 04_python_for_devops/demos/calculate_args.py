#!/usr/bin/env python3
import sys


def calculate(x, y, op):
    if op == '+':
        return x + y
    elif op == '-':
        return x - y

print('In the module', __name__)

if __name__ == '__main__':
    x, y, op = sys.argv[1:]
    print('Arguments are: ', sys.argv)
    result = calculate(int(x), int(y), op)
    print('Result is:', result)
